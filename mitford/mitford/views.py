
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.conf import settings
import json


def getFinalSchema():
	'''
	Get the representation of the final database schema to the shown in the web page.
	'''
	toReturn = '[{text:"Configuration template not found!!!"}]'
	with open(settings.SCHEMA_TEMPLATE,'r') as reader:
		toReturn = json.load(reader)
	return json.dumps(toReturn)

def index(request):
    template = loader.get_template('mitford.html')
    # context = RequestContext(request,{
    #                                  'dataSchema':getFinalSchema()
    #                                   })
    #
    # return HttpResponse(template.render(context))
    print(getFinalSchema())
    html = template.render({'dataSchema':getFinalSchema()},request)
    return HttpResponse(html)

