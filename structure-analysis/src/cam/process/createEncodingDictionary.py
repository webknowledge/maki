###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import codecs
import json
import logging
import os

import sys
from collections import defaultdict

from cam.dictionary.xpathtextdictionary import XPathTextDictionary
from cam.serialisers.fileSerializer import FileSerializer

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"

logger = logging.getLogger("EncodingDictionaryCreation")


# This script creates an empty encoding file from either the translation or the dictionary file
# Should be used with care as it may override some useful information, as custom encodings

def loadDictionary(dictionaryName):
    _serializer = FileSerializer(dictionaryName)
    dictionary = XPathTextDictionary(True, 0.7)
    dictionary.load(_serializer, None)
    _serializer.close()
    return dictionary


def saveEncodingDictionary(dictionaryName, dictionary):
    representation = defaultdict()
    representation["keySet"] = []
    representation["ignoreSet"] = []
    representation["encoding"] = dictionary.textEncoding()

    representations = [representation]

    with codecs.open(dictionaryName, "w+", "utf-8") as ifile:
        ifile.write(json.dumps(representations, sort_keys=False, indent=2))


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    translatedFile = "res/{}/data.translated.json".format(lang)
    dataFile = "res/{}/data.json".format(lang)
    encodingFile = "res/{}/encoding.json".format(lang)

    if os.path.isfile(encodingFile):
        logger.error("Encoding file exists = %s. This script does not override the file. "
                     "Please delete the file and try again ...", encodingFile)
        sys.exit(1)

    logger.info("Using language = %s, translatedFilename=%s dataFilename=%s", lang, translatedFile, dataFile)

    if os.path.isfile(translatedFile):
        _corporaDict = loadDictionary(translatedFile)
        logger.info("Loaded translated file as input ...")
    else:
        _corporaDict = loadDictionary(dataFile)
        logger.info("Loaded data file as input ...")

    saveEncodingDictionary(encodingFile, _corporaDict)
