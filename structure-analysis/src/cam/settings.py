###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


class ApplicationSettings:
    # these are some global settings to be set when some of the functions do not accept parameters

    # DEFAULTS: These properties are usually configured somewhere else
    # this is the default path comparison threshold for comparing two related paths
    DEFAULT_pathThreshold = 1
    # threshold use to decide if two words (tokens) are "synonyms"
    DEFAULT_tokenThreshold = 0.7
    # threshold use to decide if two lists of words (tokens) are "synonyms"
    DEFAULT_listSimilarityThreshold = 0.7

    # PROPERTY: This is the only place where this property can be tuned
    # the maximum number of tokes to be considered by the encoding dictionary.
    # Very long sentences are computationally expensive to deal with and they are probably not a key
    # and most certainly will be values
    PROPERTY_max_tokens = 25
    # The lengths similarity is computed as an absolute distance between two lists of tokens. If the length is larger
    # than this threshold, most certainly the list similarity would not return something relevant
    PROPERTY_len_similarity_threshold = 4
    # complex path encapsulates attributes into the path. This property helps in some cases
    PROPERTY_complexPath = True
    # the attribute find order. May be modified accordingly
    PROPERTY_complexAttributes = ["class", "id", "name"]
    # translation frequency threshold
    PROPERTY_translationFrequencyThreshold = 4
    # translation word threshold
    PROPERTY_translationWordThreshold = 4
