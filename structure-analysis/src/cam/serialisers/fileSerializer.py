###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import codecs
import json
import os

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


class FileSerializer:
    """
    A helper class to serialize and deserialize encoding dictionaries to JSON files
    """

    def __init__(self, filename):
        self.filename = filename
        self.representation = []

    def serialize(self, data):
        self.representation.append(data)

    def deserialize(self, object_id):
        if len(self.representation) == 0:
            self._loadRepresentation()
        return self.representation[0]

    def deserialize_object(self, lookup):
        if len(self.representation) == 0:
            self._loadRepresentation()

        if len(self.representation) == 0:
            return None

        if lookup is None:
            return self.representation[0]

        for document in self.representation:
            allFound = True
            for (key, value) in lookup.items():
                if document.get(key) != value:
                    allFound = False
                    break
            if allFound:
                return document
        return None

    def count(self):
        pass

    def cleanup(self):
        if os.path.isfile(self.filename):
            os.remove(self.filename)
        return True

    def close(self):
        with codecs.open(self.filename, "w+", "utf-8") as ifile:
            ifile.write(json.dumps(self.representation, sort_keys=True, indent=2))

    def _loadRepresentation(self):
        with codecs.open(self.filename, "r+", "utf-8") as ifile:
            self.representation = json.loads(ifile.read())
