# Introduction
The Multi-language Unstructured Data Extraction (MUDE)  is a tool to support an (semi-)automatic HTML to XML conversion of set of given files, having flat or hierarchical (key, value) structures, without making many assumptions over the structure of the key set. It works well with HTML tables and section (and sub-section) structures. 

The structure analysis project takes a series of html documents as input and performs a frequency analysis on all the HTML paths to determine which one may may become the keys for the conversion algorithms.

The project performs the analysis and the conversion, if an encoding dictionary has been generated successfully.

The current implementation assumes the input documents are HTML 5 valid, and all the non-relevant information is filtered: scripts, css information, comments, etc.

The output of the conversion algorithm is a valid XML file, which can serve as an input to the parsing project.

The conversion part of this library is already integrated with the parsing project.

# Get the code

```bash
git clone https://gitlab.com/digiwhist/structure-analisys.git
```

# Setup 
The current solution has been designed using python 3.4. Previous versions may not work properly. 

Apart from having a valid python 3.4 (preferably dev package) instance installed on your machine, this tutorial assumes that **pip** is also installed.  

On Ubuntu, this can be easily achieved by installing *python-virtualenv python3.4-dev libxml2-dev*.

```bash
apt-get install python-virtualenv python3.4-dev libxml2-dev
```

**Optional:** To ease the running process, a virtual environment can be created, which will isolate this project from other installs. 
In a Linux environment, the virtual environment setup can be done with: 
```bash
virtualenv env -p python3.4
source env/bin/activate
pip install -r dependencies.txt
```

If the virtual environment has not been setup, the dependencies can still be installed either manually or via the pip install command:

```bash
pip install -r dependencies.txt
```

# Short tutorial

This project assumes the following structure of the corpora, for a given country code. In general, the country code has to match the one given as a parameter to the processing scripts.

* *res/***{country}***/raw/* - contains the raw html documents (e.g. downloaded from the document archive)
* *res/***{country}***/converted/* - will be created during the conversion process, and will contain the resulted files
* *res/***{country}***/data.json* - this file is created by the frequency analysis task and will contain the preliminary dictionary encoding
* *res/***{country}***/data.translated.json* - this file is created by the translation process
* *res/***{country}***/data.csv* - this file is created by the dictionary creation task to support the key review process
* *res/***{country}***/encoding.json* - this is the result of the analysis process and will be used by the conversion process

## Frequency Analysis
The first step of this process is to create the first frequency analysis structure, which will create the data.json file.

This is a sequential process and may take a long time to conclude, depending on the number and quality of the documents.

To run this on the sample corpora (Portugal, country code: pt) execute the following:

```bash
nohup script/start-freq.sh pt > frequency.pt.log &
```

This will create a background, daemon-like process, which can be stopped by killing the process with the given **pid**. 

## Translation
Once the first step is concluded, the **optional** translation process can be started. 
This step requires a Microsoft AZURE API key, which can be configured into *cam/process/batchFrequencyTranslate.py*

The client id and secret can be obtained by logging into your Microsoft API account: https://datamarket.azure.com/developer/applications/

```bash
nohup script/start-translate.sh pt > translate.pt.log &
```

## Encoding review
Once the previous step is concluded, the encoding dictionary can be generated.

The target country code can be configured directly into the start-dictionaries.sh script.

```bash
nohup script/start-dictionaries.sh > dictionaries.pt.log &
```

The dictionary is a json file, which contains the encoding property, a keySet and an ignoreSet.

The keys can be added either manually or automatically into the keySet or, if an invalid information is found during the conversion process, into the ignoreSet.


## Data conversion (HTML -> XML)
Once the encoding has been reviewed, the conversion process can start. 
 
```bash
nohup script/start-conv.sh pt > conversion.pt.log &
```

The output can be found in the *res/***pt***/converted/* folder.


# Advanced settings

The *cam\settings.py* file contains a series of parameters that can be tuned to improve the conversion process.