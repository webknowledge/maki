from django.db import models
from mongoengine import Document,StringField,URLField,FileField,DateTimeField,connect
from django.conf import settings



connect(settings.ARCHIVE_DATABASE['archive'],
        host=settings.ARCHIVE_DATABASE['host'],
        port=settings.ARCHIVE_DATABASE['port']
        )

class ArchiveItem(Document):
    source = StringField(max_length=10,null=False)
    date = DateTimeField(null=False)
    url = StringField(null=False)
    contentType = StringField(null=False)
    file = FileField(null=False)



class FailedProcessingFiles(models.Model):
    '''
    We maintain a list of those elements in the archival that could not been processed.
    '''
    #The id corresponds to the source archival id
    archival_id=models.TextField(null=False)
    source=models.CharField(max_length=20,null=False)
    path=models.TextField(null=True)
    processed_date=models.DateTimeField(null=True)
    reason=models.TextField(null=True)

class IgnoredProcessingFiles(models.Model):
    '''
    We maintain a list of those elements we have just ignored
    '''
    #The id corresponds to the source archival id
    archival_id=models.TextField(null=False)
    source=models.CharField(max_length=20,null=False)
    path=models.TextField(null=True)
    processed_date=models.DateTimeField(null=False)
    reason=models.TextField(null=True)

class ProcessedCrawlingItems(models.Model):
    '''
    List of items stored into the archival that were processed
    '''
    archival_id=models.TextField(null=False)
    source=models.CharField(max_length=20,null=False)
    processed_date=models.DateTimeField(null=True)
    url=models.URLField(null=False)
    #Indicate if the crawling item was completely parsed
    completed=models.NullBooleanField(null=False)


'''
Decorator class to use in conjunction with Mitford for the population of the mapping schema.
isMapped indicates if the model has to be included into the showed mapping schema
pointingTable: indicates the name of the variable that has to be ignored in the schema
               because it is propagated from the parent entry.
'''
class MitfordEntry(object):
    def __init__(self,**kwargs):
        self.conf = kwargs
    def __call__(self,cls):
        cls._isMapped=self.conf["isMapped"]
        cls._pointingTable=self.conf["pointingTable"]
        return cls



#---------------------------------------
# Write your DB schema here!
#---------------------------------------
VERSION=1

@MitfordEntry(isMapped=True,pointingTable=None)
class Weather(models.Model):
    '''
    Simple weather entry
    '''
    city = models.TextField(null=False)
    temperature = models.FloatField(null=False)
    update = models.DateTimeField(null=False)
