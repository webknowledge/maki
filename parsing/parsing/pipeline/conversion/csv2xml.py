import codecs
import csv
import logging
from yattag import Doc, indent
from parsing.pipeline.conversion.base2xml import Base2Xml
import codecs
logger = logging.getLogger("Csv2Xml")


class Csv2Xml(Base2Xml):
    def processSingleData(self, filename):
        processingResult = []
        header = []
        rowNum = 0

        try:
            with codecs.open(filename, 'r', encoding="utf8") as fh:
                fh.readlines()
            csvData = csv.reader(open(filename, encoding="utf8"), delimiter='^', quotechar=None)
            #csvData = csv.reader(open(filename), delimiter='^', quotechar=None)
        except UnicodeDecodeError:
            csvData = csv.reader(open(filename, encoding="latin1"), delimiter='^', quotechar=None)
            #csvData = csv.reader(open(filename), delimiter='^', quotechar=None)

        previousRow = []

        for row in csvData:
            if rowNum == 0:
                header = [item.replace(' ', '_') for item in row]
                rowNum += 1
            else:
                outFilename = self.getOutputFilename(filename, rowNum)
                if len(previousRow) > 0:
                    if len(row) > 0:
                        previousRow[-1] = previousRow[-1] + " " + row[0]
                        previousRow.extend(row[1:])
                    row = previousRow

                extraHeader, extraValue = self.getExtraFields(filename)

                if self._processSingleRow(outFilename, extraHeader, extraValue, header, row, rowNum):
                    processingResult.append(outFilename)
                    previousRow = []
                    rowNum += 1
                    logger.info("Finished writing: " + outFilename)
                else:
                    if len(row) > len(header):
                        # something went very wrong here
                        logger.error("Row larger than header: %s", row)
                        previousRow = []
                    else:
                        previousRow = list(row)

        return processingResult

    @staticmethod
    def _processSingleRow(outFilename, extraHeader, extraValue, header, row, rowIndex):
        if len(header) != len(row):
            logger.warning("Row length different than the header: row != header (%s != %s), index = %s @ %s",
                           len(row), len(header), rowIndex, row)
            return False
        doc, tag, text = Doc().tagtext()
        with tag("document"):
            for i in range(len(extraHeader)):
                with tag(extraHeader[i]):
                    text(extraValue[i])

            for i in range(len(header)):
                with tag(header[i]):
                    text(row[i])

        #with codecs.open(outFilename, "w+", "utf-8") as ifile:
        with open(outFilename, "w+") as ifile:
            ifile.write(indent(doc.getvalue(), indentation=' ' * 4, newline='\n'))
        return True
