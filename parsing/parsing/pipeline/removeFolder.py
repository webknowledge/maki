
import os
import shutil


import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class DataProcessor(object):
    '''
    Remove the uncompressed TED file after processing.
    '''
    
    def process_item(self,item):
        for entry,archivalId,url in item['folder']:
            self.removePath(entry)      
                    
        #and now remove any folder or whatever
        #for x in glob.glob(settings.INTERMEDIATE_FOLDER+"/*"):
        #    self.removePath(x)
    
    
    def removePath(self,entry):
        if os.path.isdir(entry):
            logger.debug("Removed ["+entry+']')
            try:
                shutil.rmtree(entry)
            except Exception as e:
                logger.error("Impossible to remove "+entry)
                logger.error(str(e))
                    
        else:
            try:
                os.remove(entry)
            except Exception as e:
                logger.error("Impossible to remove "+entry)
                logger.error(str(e))