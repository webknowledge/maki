import math
from nltk.metrics.distance import edit_distance


def cosine_similarity(document1, document2):
    allKeys = set(document1.keys())
    allKeys.update(document2.keys())

    sumxx = 0
    sumyy = 0
    sumxy = 0

    for key in allKeys:
        x = document1.get(key, 0)
        y = document2.get(key, 0)
        sumxx += x * x
        sumyy += y * y
        sumxy += x * y
    return sumxy / math.sqrt(sumxx * sumyy)


def tfidf(frequency_document, dicti, corpora_size):
    frequency_map = frequency_document.frequencies()
    tf_idf_map = frequency_document.tf_idf()
    for key in frequency_map.keys():
        tf = frequency_map.get(key) / frequency_document.size()
        tf_idf_map[key] = tf * idf(key, dicti, corpora_size)


def idf(term, dicti, corpora_size):
    return math.log(corpora_size / (1 + dicti.n_frequencies(term)))


# Edit distance-based similarities for tokens and sets
def similarity_token(token1, token2):
    return 1 - (float(edit_distance(token1.lower(), token2.lower())) / max(len(token1), len(token2), 1))


def similarity_sets(set1, set2, token_similarity_threshold):
    return 1 - (float(_edit_distance_array(set1, set2, token_similarity_threshold)) / max(len(set1), len(set2), 1))


def len_similarity_sets(set1, set2):
    min_len = min(len(set1), len(set2))
    max_len = max(len(set1), len(set2))
    return max_len - min_len


def similarity_path(path1, path2):
    n = min(len(path1), len(path2))
    count = 0
    for i in range(n):
        if path1[i] == path2[i]:
            count += 1
        else:
            break
    return max(len(path1), len(path2)) - count


# copied from nltk.metrics.distance and modified to work with lists/arrays/sets
def _edit_dist_init(len1, len2):
    lev = []
    for i in range(len1):
        lev.append([0] * len2)  # initialize 2D array to zero
    for i in range(len1):
        lev[i][0] = i  # column 0: 0,1,2,3,4,...
    for j in range(len2):
        lev[0][j] = j  # row 0: 0,1,2,3,4,...
    return lev


def _edit_dist_step_array(lev, i, j, s1, s2, token_similarity_threshold):
    c1 = s1[i - 1]
    c2 = s2[j - 1]

    # skipping a character in s1
    a = lev[i - 1][j] + 1
    # skipping a character in s2
    b = lev[i][j - 1] + 1
    # substitution
    c = lev[i - 1][j - 1] + (similarity_token(c1, c2) < token_similarity_threshold)

    # transposition
    d = c + 1  # never picked by default

    # pick the cheapest
    lev[i][j] = min(a, b, c, d)


def _edit_distance_array(set1, set2, token_similarity_threshold):
    # set up a 2-D array
    len1 = len(set1)
    len2 = len(set2)
    lev = _edit_dist_init(len1 + 1, len2 + 1)

    # iterate over the array
    for i in range(len1):
        for j in range(len2):
            _edit_dist_step_array(lev, i + 1, j + 1, set1, set2, token_similarity_threshold)
    return lev[len1][len2]


if __name__ == "__main__":
    print(similarity_sets(["a", "aa"], ["bb", "aab"], 0.9))
    print(similarity_token("abcd", "bb"))
    print(edit_distance("abcd", "bb", False))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h", "h1", "etc"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h", "h1"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h", "h1", "ec"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h1", "kk"]))
