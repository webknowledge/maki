class DataRescue:
    def rescueTag(self, tag):
        pass

    def isTagRescuable(self, tag):
        return False

    def isRootChangeable(self, tag):
        return False

    def changeRoot(self, tag):
        return tag

    def canProcessNonStandard(self):
        return False

    def parseNonStandard(self, tag):
        return None

    def isTextRescuable(self, path, text):
        for tag in ["title", "h1", "h2", "h3"]:
            if tag in path:
                return True
        return False

    def rescueText(self, path, text):
        for tag in ["title", "h1", "h2", "h3"]:
            if tag in path:
                return tag, text
        return None


class CzRescuer(DataRescue):
    def rescueTag(self, tag):
        if tag.name == "fieldset":
            return self._rescueFieldset(tag)
        elif tag.name == "input":
            return self._rescueInputText(tag)
        elif tag.name == "select":
            return self._rescueSelectValue(tag)

    def isTagRescuable(self, tag):
        if tag.name == "fieldset" and "class" in tag.attrs and "form" in tag.attrs.get("class"):
            for itemType in ["checkbox", "radio"]:
                itemTypeList = tag.findAll("input", {"type": itemType})
                if itemTypeList is not None and len(itemTypeList) > 0:
                    return True
        elif tag.name == "input" and "class" in tag.attrs and "input-readonly" in tag.attrs.get("class") and \
                        "type" in tag.attrs and "text" in tag.attrs.get("type") and "value" in tag.attrs:
            return True
        elif tag.name == "select":
            return True
        return False

    @staticmethod
    def _rescueFieldset(tag):
        result = []

        for itemType in ["checkbox", "radio"]:
            itemTypeList = tag.findAll("input", {"type": itemType})
            if itemTypeList is not None:
                for inputItem in itemTypeList:
                    if inputItem.attrs.get("checked") is not None and "checked" in inputItem.attrs.get("checked"):
                        inputItemId = inputItem.attrs.get("id")
                        labels = tag.findAll("label", {"for": inputItemId})
                        if labels is not None:
                            for labelItem in labels:
                                result.append(labelItem.text)

        return ";".join(result)

    @staticmethod
    def _rescueInputText(tag):
        inputValue = tag.attrs.get("value")
        if inputValue is not None:
            return inputValue
        else:
            return ""

    @staticmethod
    def _rescueSelectValue(tag):
        result = []
        selectedValues = tag.findAll("option", {"selected": "selected"})
        if selectedValues is not None:
            for selectedValue in selectedValues:
                result.append(selectedValue.text)
        return ";".join(result)


class PtRescuer(DataRescue):
    def isRootChangeable(self, tag):
        return True

    def changeRoot(self, tag):
        newRoots = tag.findAll("div", {"template": "base/pt/Pesquisa/Contrato.php"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        newRoots = tag.findAll("div", {"template": "base/pt/Pesquisa/Anuncio.php"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        return tag


class LtRescuer(DataRescue):
    def isRootChangeable(self, tag):
        return True

    def changeRoot(self, tag):
        newRoots = tag.findAll("div", {"class": "content"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        return tag


class EeRescuer(DataRescue):
    def isTextRescuable(self, path, text):
        if self._isContractType(path, text):
            return True
        return super().isTextRescuable(path, text)

    def rescueText(self, path, text):
        if self._isContractType(path, text):
            return "contractType", text
        else:
            return super().rescueText(path, text)

    @staticmethod
    def _isContractType(path, text):
        if len(path) > 0 and path[-1] == "font":
            for item in ["Riigihanke aruanne", "Hanketeade"]:
                if item in text:
                    return True
        return False


class ChRescuer(DataRescue):
    def isRootChangeable(self, tag):
        return True

    def changeRoot(self, tag):
        newRoots = tag.findAll("div", {"class": "preview"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        return tag

    def canProcessNonStandard(self):
        return True

    def parseNonStandard(self, tag):
        result = dict()

        headerTag = tag.findAll("div", {"class": "result_head"})
        if headerTag is not None and len(headerTag) > 0:
            headerTagSplit = headerTag[0].text.split("|")
            result["publicationDatePortal"] = headerTagSplit[0]
            result["projectId"] = headerTagSplit[1].split(" ")[-1]
            result["announcementId"] = headerTagSplit[2].split(" ")[-1]
            result["announcementType"] = headerTagSplit[3]

        return result


class BgRescuer(DataRescue):
    def canProcessNonStandard(self):
        return True

    def parseNonStandard(self, tag):
        result = dict()

        headerTag = tag.findAll("div", {"class": "stdoc"})
        if headerTag is not None and len(headerTag) > 0:
            counter = 0
            for item in headerTag:
                result["stdoc-line-" + str(counter)] = item.text.strip()
                counter += 1
        cpvTag = tag.findAll("span",{"class":"txcpv"})
        if cpvTag is not None and len(cpvTag) > 0:
            aux=""
            for item in cpvTag:
                aux=aux+" "+item.text.strip()
            result['cpv']=aux
        return result

    def isTextRescuable(self, path, text):
        if self._isContractNumber(path, text):
            return True
        else:
            return super().isTextRescuable(path, text)

    def rescueText(self, path, text):
        if self._isContractNumber(path, text):
            return "contractNumber", text
        else:
            return super().rescueText(path, text)

    @staticmethod
    def _isContractNumber(path, text):
        if len(path) > 1 and path[-1] == "p" and path[-2] == "font":
            return True
        return False


class HrRescuer(DataRescue):
    def isTagRescuable(self, tag):
        if self._ignoreDiv(tag):
            return True
        else:
            return super().isTagRescuable(tag)

    def rescueTag(self, tag):
        if self._ignoreDiv(tag):
            return ""
        else:
            return super().rescueTag(tag)

    def canProcessNonStandard(self):
        return True

    def parseNonStandard(self, tag):
        result = dict()

        headerTag = tag.findAll("div", {"class": "FormHeader"})
        if headerTag is not None and len(headerTag) > 0:
            result["announcementType"] = headerTag[0].text.strip()
            if len(result["announcementType"]) == 0:
                result["announcementType"] = "Objava - Koncesije"
        return result

    @staticmethod
    def _ignoreDiv(tag):
        if tag.name == "div" and "class" in tag.attrs and "FormHeader" in tag.attrs.get("class"):
            return True
        else:
            return False
