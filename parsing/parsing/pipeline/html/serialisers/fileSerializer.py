import codecs
import json
import os


class FileSerializer:
    def __init__(self, filename):
        self.filename = filename
        self.representation = []

    def serialize(self, data):
        self.representation.append(data)

    def deserialize(self, object_id):
        if len(self.representation) == 0:
            self._loadRepresentation()
        return self.representation[0]

    def deserialize_object(self, lookup):
        if len(self.representation) == 0:
            self._loadRepresentation()

        if len(self.representation) == 0:
            return None

        if lookup is None:
            return self.representation[0]

        for document in self.representation:
            allFound = True
            for (key, value) in lookup.items():
                if document.get(key) != value:
                    allFound = False
                    break
            if allFound:
                return document
        return None

    def count(self):
        pass

    def cleanup(self):
        if os.path.isfile(self.filename):
            os.remove(self.filename)
        return True

    def close(self):
        with codecs.open(self.filename, "w+", "utf-8") as ifile:
            ifile.write(json.dumps(self.representation, sort_keys=True, indent=2))

    def _loadRepresentation(self):
        with codecs.open(self.filename, "r+", "utf-8") as ifile:
            self.representation = json.loads(ifile.read())
