# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup, Tag, NavigableString, Comment, CData, ProcessingInstruction, Declaration
import re
from nltk import regexp_tokenize, WordPunctTokenizer

regexp_individual_patterns = [
    u'(?:\d+[\-])+\d+',  # phone numbers
    u'(?:\d+[\,\.])+\d*',  # numbers, todo; add known currencies
    u'(?:http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)',  # urls
    u'(?:[^@|\s]+@[^@]+\.[^@|\s]+)',  # e-mail
    u'\w\'',  # french definite articles, and first person singular pronouns
    u'\w+',  # alpha numeric sequences
    u'\S+'  # non-whitespace characters
]
globalRegexpPattern = u"|".join(regexp_individual_patterns)

wordPunctTokenizerObject = WordPunctTokenizer()


def cleanupText(item):
    result = item.strip()
    result = re.sub("^[MDCLXVI]+[.]([0-9]+[.)])+", " ", result)
    result = re.sub("^([0-9]+[.])*[0-9]+[)]", " ", result)
    result = re.sub("(?i)^section [MDCLXVI]+", " ", result)
    result = re.sub("(?i)^section [0-9]+", " ", result)
    result = re.sub("(^[).;,])|([(]$)", " ", result)
    result = re.sub("\n", " ", result)
    result = re.sub("\t", " ", result)
    result = re.sub("[ ]+", " ", result)
    return result.strip()


def cleanupPath(pathList):
    return [item for item in pathList if item not in ["a"]]


def parent_path(tag):
    result = []
    for item in tag.parents:
        if item is not None:
            result.insert(0, item.name)
    return result


def xpath(tag, tag_text):
    return parent_path(tag), tag_text


def traverseDelegate(tag, delegateFunction, dataRescuer):
    if isinstance(tag, NavigableString) and not isinstance(tag, Comment) and not isinstance(tag, CData) \
            and not isinstance(tag, ProcessingInstruction) and not isinstance(tag, Declaration):
        tag_text = cleanupText(tag.string)
        if len(tag_text) > 0:
            (xtagpath, text) = xpath(tag, tag_text)
            # sometimes the script is misinterpreted as text by the parser
            if "script" not in xtagpath:
                _addPath(delegateFunction, xtagpath, text)
    elif isinstance(tag, Tag):
        for item in tag.children:
            if dataRescuer is not None and dataRescuer.isTagRescuable(item):
                rescuedItem = dataRescuer.rescueTag(item)
                if len(rescuedItem) > 0:
                    (xtagpath, text) = xpath(item, rescuedItem)
                    _addPath(delegateFunction, xtagpath, text)
            else:
                traverseDelegate(item, delegateFunction, dataRescuer)


def traverseToDictionary(tag, frequencyDictionary):
    traverseDelegate(tag, frequencyDictionary.add, None)


def _addPath(delegateFunction, xtagpath, text):
    if ":" in text:
        text = re.sub("http:", "http~", text)
        text = re.sub("https:", "https~", text)
        textList = text.split(":")
    else:
        textList = [text]

    for item in textList:
        item = re.sub("http~", "http:", item)
        item = re.sub("https~", "https:", item)
        clean_item = cleanupText(item)
        if len(clean_item) > 0:
            delegateFunction(cleanupPath(xtagpath), clean_item)


def fixEmail(rawDocument):
    email_fix = re.sub("[<]span[ ]+class[=]([']|[\"])arobas([']|[\"])[>][^<]+?[<][/]span[>]", "@", rawDocument)
    email_fix = re.sub("[<]span[ ]+class[=]([']|[\"])arobas([']|[\"])[/][>]", "@", email_fix)
    return email_fix


def fixEmailInDocument(inputDocument):
    return fixEmail(inputDocument.read())


def _valid(token):
    return token not in [":", ".", ",", "?", "!", "-", "=", "~"]


def regexpTokenize(line):
    tokens = regexp_tokenize(line, pattern=globalRegexpPattern)
    tokens = [token for token in tokens if _valid(token)]
    return tokens


def wordPunctTokenizer(line):
    tokens = wordPunctTokenizerObject.tokenize(line)
    tokens = [token for token in tokens if _valid(token)]
    return tokens

# todo; move this piece of code somewhere else to prevent cyclic imports
# if __name__ == "__main__":
#     input_file = "res/html_data/11-152706-38.html.main.out"
#     output_file = "res/html_data/11-152706-38.html.converted.xml"
#
#     exporter = XmlExporter()
#     dictionary = XPathTextDictionary(True)
#     with codecs.open(input_file, "r+", "utf-8") as ifile:
#         soup = BeautifulSoup(fixEmailInDocument(ifile), 'html5lib')
#         traverse(soup, dictionary)
#         # for (key, value) in sorted(dictionary.encoding().items(), key=lambda x: x[1]):
#         #     print(key + " => " + str(value))
#     with codecs.open(output_file, "w+", "utf-8") as ofile:
#         ofile.write(exporter.export(dictionary))
