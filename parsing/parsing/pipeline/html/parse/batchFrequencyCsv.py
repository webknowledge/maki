import codecs
import os

import sys

import logging

from DigiwhistWeb.parsing.pipeline.html.dictionary.xpathtextdictionary import XPathTextDictionary
from DigiwhistWeb.parsing.pipeline.html.serialisers.fileSerializer import FileSerializer

logger = logging.getLogger("FrequencyCSVExport")


def loadDictionary(dictionaryName):
    mongo_serializer = FileSerializer(dictionaryName)
    dictionary = XPathTextDictionary(True, 0.7)
    dictionary.load(mongo_serializer, None)
    mongo_serializer.close()
    return dictionary


def exportTextFrequencies(csvFilename, corporaDict, frequencyThreshold):
    with codecs.open(csvFilename, "w+", "utf8") as ofile:
        for (encoding, frequency) in corporaDict.textFrequencies().items():
            if frequency >= frequencyThreshold:
                line = "\";\"".join([" ".join(tokens) for tokens in corporaDict.getTranslationOrText(encoding)])
                line = str(encoding) + ";" + str(frequency) + ";\"" + line + "\""
                ofile.write(line + "\n")


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    translatedFile = "res/{}/data.translated.json".format(lang)
    dataFile = "res/{}/data.json".format(lang)
    outFile = "res/{}/data.csv".format(lang)

    logger.info("Using language = %s, translatedFilename=%s dataFilename=%s outFile=%s",
                lang, translatedFile, dataFile, outFile)

    if os.path.isfile(translatedFile):
        _corporaDict = loadDictionary(translatedFile)
        logger.info("Loaded translated file as input ...")
    else:
        _corporaDict = loadDictionary(dataFile)
        logger.info("Loaded data file as input ...")

    exportTextFrequencies(outFile, _corporaDict, 5)
