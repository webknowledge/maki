import codecs
from glob import glob
from bs4 import BeautifulSoup
import time
import random
import logging
import sys

from DigiwhistWeb.parsing.pipeline.html.dictionary.xpathtextdictionary import XPathTextDictionary
from DigiwhistWeb.parsing.pipeline.html.parse.simpleParsing import fixEmailInDocument, traverseToDictionary
from DigiwhistWeb.parsing.pipeline.html.serialisers.fileSerializer import FileSerializer

logger = logging.getLogger("FrequencyAnalysis")


def computeCorporaEncoding(corpora, outFile):
    dictionary = XPathTextDictionary(True, 0.7)
    fileIndex = 0
    for f in corpora:
        with codecs.open(f, "r+", "utf-8") as ifile:
            soup = BeautifulSoup(fixEmailInDocument(ifile), 'html5lib')
            traverseToDictionary(soup, dictionary)
        logger.info("Processed file = %s", f)
        fileIndex += 1
        if fileIndex % 10 == 0:
            saveEncoding(dictionary, outFile)
    return dictionary


def saveEncoding(dictionary, outFile):
    mongo_serializer = FileSerializer(outFile)
    mongo_serializer.cleanup()
    dictionary.save(mongo_serializer)
    mongo_serializer.close()
    logger.info("Encoding file saved = %s", outFile)


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    inFolder = "res/{}/raw/*".format(lang)
    _outFile = "res/{}/data.json".format(lang)

    logger.info("Using input folder = %s", inFolder)
    logger.info("Using output folder = %s", _outFile)

    globList = glob(inFolder)

    corpora_list = random.sample(globList, min(100, len(globList)))

    logger.info("Using %s random samples ...", len(corpora_list))

    tic = time.clock()
    corpora_dict = computeCorporaEncoding(corpora_list, _outFile)
    saveEncoding(corpora_dict, _outFile)
    print(time.clock() - tic)
