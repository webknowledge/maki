import logging
import os

import sys
from microsofttranslator import Translator

import time

from parsing.pipeline.html.dictionary.xpathtextdictionary import XPathTextDictionary
from parsing.pipeline.html.serialisers.fileSerializer import FileSerializer

logger = logging.getLogger("FrequencyTranslate")

frequencyThreshold = 4
wordThreshold = 4
error = False


def loadDictionary(dictionaryName):
    mongo_serializer = FileSerializer(dictionaryName)
    dictionary = XPathTextDictionary(True, 0.7)
    dictionary.load(mongo_serializer, None)
    mongo_serializer.close()
    return dictionary


def translate(corporaDict, originalLang):
    translator = Translator('translation-test', 'C7JRh1XxBcKIfHxFc83KuBaWZf5S/ycxAiODW26eRDw=')
    for (encoding, synsets) in corporaDict.textEncoding().items():
        if (not corporaDict.hasTranslation(encoding)) and \
                (corporaDict.encodingFrequency(encoding) > frequencyThreshold):
            for synset in synsets:
                line = "\"" + " ".join(synset) + "\""
                if len(line) > wordThreshold:
                    logger.debug("Translating line: " + line)
                    try:
                        translatedLine = translator.translate(line, "en")
                    except Exception as e:
                        logger.error("Had some error : " + str(e))
                        global error
                        error = True
                        return
                    logger.info("[%s] %s => [EN] %s", originalLang, line, translatedLine)
                    corporaDict.addTranslation(encoding, translatedLine)
                else:
                    logger.info("Skipped line=%s, line too short, engine will raise an error ...", line)
        else:
            logger.info("Skipped key=%s, translation exists or very low frequency ...", encoding)


def saveDictionary(dictionaryName, dictionary):
    mongo_serializer = FileSerializer(dictionaryName)
    dictionary.save(mongo_serializer)
    mongo_serializer.close()


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    translatedFile = "res/{}/data.translated.json".format(lang)
    dataFile = "res/{}/data.json".format(lang)

    logger.info("Using language = %s, translatedFilename=%s dataFilename=%s", lang, translatedFile, dataFile)

    tic = time.clock()

    if os.path.isfile(translatedFile):
        _corporaDict = loadDictionary(translatedFile)
        logger.info("Loaded translated file as input ...")
    else:
        _corporaDict = loadDictionary(dataFile)
        logger.info("Loaded data file as input ...")

    translate(_corporaDict, lang)
    saveDictionary(translatedFile, _corporaDict)
    print(time.clock() - tic)

    if error:
        sys.exit(1)
    else:
        sys.exit(0)
