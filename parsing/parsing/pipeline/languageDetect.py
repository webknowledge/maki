
import logging
import codecs
import langid

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    def process_item(self, item):
        item["file-language"] = []
        for inputFile, archival, url in item['folder']:
            with codecs.open(inputFile, "r+", "utf-8") as ifile:
                language = langid.classify(ifile.read())[0]
                item["file-language"].append(language)

        return item
