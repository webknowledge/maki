
from django.core.management.base import BaseCommand
from parsing.parser import MappingProcessor

from django.conf import settings

import multiprocessing

import logging
   
from parsing.models import ArchiveItem,ProcessedCrawlingItems

# Get an instance of a logger
logger = logging.getLogger("MainLogger")


def preprocessArchived(item):
    '''
    Launch the corresponding pipeline for an archived item
    '''
    previousResult=item
    for p in settings.PREPROCESS_PIPELINES[item['source']]:
        logger.debug("Apply preprocess "+p)
        module = __import__(p,fromlist=["DataProcessor"])
        instance = module.DataProcessor()
        try:
            previousResult=instance.process_item(previousResult)
        except Exception as e:
            logger.error("There was an error during preprocessing")
            logger.error(str(e))
            previousResult=None
            break
    return previousResult

class Command(BaseCommand):
    
    help = 'Parse the elements found in the archive for a given datasource'

    BATCH_SIZE=100
    
    def add_arguments(self,parser):
        parser.add_argument('source',type=str,help='Name of data source')
        parser.add_argument('numItems',nargs='?',type=int,help="Number of items to process",
                            default=-1)
        parser.add_argument('batchSize',nargs='?',type=int,help="Size of processing batch",
                            default=1)
        parser.add_argument('--onlyPreProcessing',help="Only run preprocessing",default=False,action="store_true")
              
    
    def handle(self, *args, **options):
        self.BATCH_SIZE=options['batchSize']
        logger.info('Configured with BATCH_SIZE=%d'%(self.BATCH_SIZE))
        self.launchParser(options['source'],options['numItems'],options['onlyPreProcessing'])
        
    def launchParser(self,source,numItems,onlyPreProcessing):

        logging.info("Looking for archive entries...")
        self.processArchiveEntries(source,numItems,onlyPreProcessing)
        


    def processArchiveEntries(self,source,numItems,onlyPreProcessing):
        parser = MappingProcessor()
        
        querySet=None
        if numItems == -1:
            logger.info("Querying database...")
            querySet = ArchiveItem.objects.filter(source=source)
        else:
            logging.info("Run a query for the first "+str(numItems)+" items")
            querySet = ArchiveItem.objects.filter(source=source)[:numItems]
            
        item = {'source':source,
                    'folder':[]
                    }    

        elementsToPreProcess=[]

        for archived in querySet:
            item={
                'source':source,
                'folder':[],
                'archivalId':archived.id,
                'url':archived.url
            }

            #was this thing already processed?
            alreadyProcessedEntries=0
            doIt=True
            if not onlyPreProcessing:
                processed = ProcessedCrawlingItems.objects.using(source).filter(archival_id=archived.id)
                alreadyProcessedEntries = processed.count()
                theCrawlingItem = None
                if alreadyProcessedEntries != 0:
                    theCrawlingItem = processed.get()
                    if theCrawlingItem.completed:
                        logger.info("The crawling item: "+str(theCrawlingItem.id)+" was already processed")
                        doIt=False
                    else:
                        logger.info("The crawling item: "+str(theCrawlingItem.id)+" was not completed")


            if doIt:
                #create the crawling item
                theCrawlingItem = ProcessedCrawlingItems()
                theCrawlingItem.archival_id=archived.id
                theCrawlingItem.source=source
                theCrawlingItem.url=archived.url
                theCrawlingItem.completed=False
                item['crawlingItem']=theCrawlingItem
                elementsToPreProcess.append(item)

            if len(elementsToPreProcess) >= self.BATCH_SIZE:
                logger.info("Number of elements to preprocess: %d"%len(elementsToPreProcess))
                preprocess_info = self.runMultiplePreProcess(elementsToPreProcess)

                if not onlyPreProcessing:
                    #save the crawling item
                    theCrawlingItem.save(using=source)

                    fileSet = []
                    for i in preprocess_info:
                        if i!=None:
                            fileSet.extend(i['folder'])

                    logger.info("Number of items to parse: %d"%len(fileSet))
                    postprocess_info = parser.process_item(fileSet,source,theCrawlingItem)
                    for item in preprocess_info:
                        if item!=None:
                            postprocess_info=self.postprocessArchived(item)
                            item['folder']=[]

                elementsToPreProcess=[]


        logger.info("Number of remaining elements to preprocess: %d"%len(elementsToPreProcess))
        if len(elementsToPreProcess)>0:
            preprocess_info = self.runMultiplePreProcess(elementsToPreProcess)
            if not onlyPreProcessing:
                fileSet = []
                for i in preprocess_info:
                    if i!=None:
                        fileSet.extend(i['folder'])

                info_to_postprocess = parser.process_item(fileSet,source,theCrawlingItem)
                for item in preprocess_info:
                    if item!=None:
                        postprocess_info=self.postprocessArchived(item)
                        item['folder']=[]

    def runMultiplePreProcess(self,elementsToPreProcess):
        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        preprocess_info = pool.map(preprocessArchived,elementsToPreProcess)
        pool.close()
        pool.join()

        return preprocess_info

      

            
    def postprocessArchived(self,item):
        '''
        Postprocessing after processing is finished. Normally this step
        cleans intermediate files.
        '''

        previousResult=item
        
        for p in settings.POSTPROCESS_PIPELINES[item['source']]:
            logger.debug("Apply postprocess "+p)
            module = __import__(p,fromlist=["DataProcessor"])
            instance = module.DataProcessor()
            previousResult=instance.process_item(previousResult)
        return previousResult