import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&55^ia0*1^0)o2t^721_98#ui1axk=8&i_&&(em26#%vcoap54'


ROOT_URLCONF='urls.py'


'''
This function initializes a large bunch of databases
'''

userDB="userDB"
passwordDB="passwordDB"
hostDB='localhost'


AVAILABLE_DATABASES=set(["weather"])


def initializeDBs(availableDBs):
    dbs={
    'default':{
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'default',
         'USER': userDB,
         'PASSWORD':passwordDB,
         'HOST': hostDB
        }
    }

    for db in availableDBs:
        dbs[db]={
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': db,
         'USER': userDB,
         'PASSWORD':passwordDB,
         'HOST': hostDB
    }
    return dbs


INSTALLED_APPS = (
    'parsing',
)



# settings.py
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    
    'formatters': {
        'verbose': {
            'format': '[%(levelname)s %(asctime)s%(filename)s:%(lineno)s ] %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
       'file':{
            'class':'logging.handlers.RotatingFileHandler',
            'formatter':'verbose',
            'filename':'/tmp/dwhist.log',
            'maxBytes':104857600
        }

    },
    'loggers': {
        'MainLogger': {
            'handlers': ['console','file'],
            'level': 'INFO',
        },
    }
}


#Mongo archive configuration
ARCHIVE_DATABASE={"archive":"weather",
                  "host":"localhost",
                  "port":27017
                  }






DATABASES=initializeDBs(AVAILABLE_DATABASES)

#Folder containing the dictionaries
DICTIONARY_FOLDER="dictionaries"

#Folder where the mapping files are stored
MAPPING_FOLDER="parsing/mappings"

#where to store intermediate created files
INTERMEDIATE_FOLDER="/tmp/intermediate"


#Pipeline definitions
PREPROCESS_PIPELINES = {
            "weather":[
                    'parsing.pipeline.getFile'
                    ]
            }

POSTPROCESS_PIPELINES = {
             "weather":[
                    'parsing.pipeline.removeFolder'
                    ]
             }





