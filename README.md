# Introduction
MAKI is a set of tools for web data knowledge. MAKI permits to define a set of tools involved in
the design of web knowledge extraction project. The implementation works on top of Django making
it really familiar to Python developers.

We have used a modified version of MAKI in a real use case extracting public procurement data from
European countries. You can check the code at https://gitlab.com/groups/digiwhist.

In the current version we offer a small example that downloads the weather prediction from
http://openweathermap.org

# Get the code
```
 git clone https://gitlab.com/webknowledge/maki.git
```

# Prepare the environment
Install system libraries before updating the python libraries. We assume you are
using a linux environment, in particular Ubuntu.
```
sudo apt-get install python-virtualenv python3.4-dev libxml2-dev libpq-dev libxslt-dev
```

MAKI is designed to work using python 3.4. However, we need to install Python 2.7
in order to make it compatible with the Scrapy library. Fortunately, we can work
with both versions using virtual wrapper.

Create the environment for Python 2.7
```
virtualenv env2.7 -p python2.7
```
Create the environment for Python 3.4
```
virtualenv env -p python3.4
```

# Configure the archive and database
In the example we assumne that a Mongo server and a PostgreSQL are running in
the local machine. However, if you want to modify the configuration files simply
go to
* crawling/settings.py to modify the Mongo used by the crawler
* parsing/settings.py to modify the Mongo used by read the incoming data
* parsing/settings.py to modify the database where the database will be finally
stored

Mongo configuration can be changed by modifying:
```python
#Mongo archive configuration
ARCHIVE_DATABASE={"archive":"archive",
                  "host":"localhost",
                  "port":27017
                  }
```
Similarly, the database configuration can be modifying changing:
```python
userDB="userDb"
passwordDB="dbPassword"
hostDB='localhost'
```
And the set of available databases in 
```python
AVAILABLE_DATABASES=set(["weather"])
```

# Step 1: Crawl the data

Activate the Python 2.7 environment and install the corresponding packages
```
source env2.7/bin/activate
pip install -r crawling/dependencies
```
Now run the crawlings for the weather example.
```
python crawling/manage.py launchCrawler weather
```
Wait until if finishes to have all the data stored into Mongo. Be sure to
deactivate the current python2.7 environment
```
deactive
```

# Step 2: Mapping the data

Now we can annotate the data to a given target database schema using the mapping
tool MITFORD. We actually provide a mapping design for demonstration purposes.

Run MITFORD locally:
```
source env/bin/activate
pip install -r mitford/dependencies
python mitford/manage.py runserver
```
Now MITFORD will be running on http://127.0.0.1:8000/mitford In that URL you 
can load an example of XML file and define the mapping. Then you can save this 
mapping into mitford/mappings/weather_mapping.json The _mapping.json ending is
quite important to assure the parsing tool finds the file.

When finalized we can press CTRL+C.

# Step 3: Parsing

Install the packages needed by the parser.
```
pip install -r parsing/dependencies
```
The database schema is defined in parsing/models.py This can be modified for
additional examples. Then, we run the parser
```
python parsing/manage.py launchParserArchive weather 10 10 
```
The parameters weather 10 10 indicate that we are parsing the data extracted 
from the weather crawler, we will stop after processing 10 files, and we are 
going to process the files in batches of 10 elements. When the system stops
the data is transformed and allocated into the database.



