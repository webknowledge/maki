from crawling.spiders.basicSpider import BasicSpider

import logging

# Get an instance of a logger
logger = logging.getLogger("MainLogger")

class TemplateSpider(BasicSpider):
    '''
    This is a minimalistic template for a spider that inherits basic functions provided by the BasicSpider.
    '''

    '''
    Name of the spider
    '''
    name='template'

    allowed_domains=['google.com']

    '''
    List of intial urls to crawl
    '''
    urls=[
        'http://www.google.com'
    ]



    def __init__(self):
        #initialize the basic spider
        BasicSpider.__init__(self,self.urls,self.name)


    def parse(self,response):
        #Simply print the url of the crawled page.
        logger.info('We have crawled: %s'%response.url)
