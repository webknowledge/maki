from crawling.spiders.basicSpider import BasicSpider

from selenium import webdriver


from django.conf import settings

from scrapy import Request

import logging

# Get an instance of a logger
logger = logging.getLogger("MainLogger")


class Spider(BasicSpider):

    name='amazon'

    allowed_domains=['amazon.co.uk']

    urls=[
        'http://www.amazon.co.uk/s/?field-keywords=drone'
    ]

    custom_settings={'DOWNLOAD_DELAY':1.15,
                     'USER_AGENT':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"
                     }


    def __init__(self):
        BasicSpider.__init__(self,self.urls,self.name)

        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any',
                                                                                 '--ignore-ssl-errors=true',
                                                                                 '--web-security=false'])

    def parse(self,response):
        '''
        For the given initial page we are going collect all the urls given in the result
        :param response:
        :return:
        '''

        #self.driver.get(response.url)
        #non sponsorized entries
        logger.info("Processing page: %s"%response.url)

        results_xpath= "//a[contains(concat(' ',normalize-space(@class),' '), 'a-link-normal s-access-detail-page a-text-normal')]/@href"

        items = response.selector.xpath(results_xpath).extract()

        #items=WebDriverWait(self.driver,10).until(EC.presence_of_all_elements_located((By.XPATH,resultsXPath)))
        #take a look at every item
        counter=0
        for i in items:
            urlToExplore = response.urljoin(i)
            yield Request(urlToExplore,self.archiveItem)
            counter+=1

        #go to the next page
        next_link = response.xpath('//a[@id="pagnNextLink"]/@href').extract()

        if len(next_link)!=0:
            yield Request(response.urljoin(next_link[0]),self.parse)

        return




