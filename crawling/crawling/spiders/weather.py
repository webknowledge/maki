from crawling.spiders.basicSpider import BasicSpider

import logging

# Get an instance of a logger
logger = logging.getLogger("MainLogger")
import datetime



class Spider(BasicSpider):
    '''
    Example of a spider getting the weather observation for several cities from
    openweathermap.org
    '''

    name='weather'

    allowed_domains=['api.openweathermap.org']

    api_string='http://api.openweathermap.org/data/2.5/weather?mode=xml&q=%s&appid=b1b15e88fa797225412429c1c50c122a'

    cities=['London','Madrid','Paris','Athens','Brussels','Berlin','Rome']

    urls=[ api_string%city for city in cities ]


    def __init__(self):
        BasicSpider.__init__(self,self.urls,self.name)



    def parse(self,response):
        '''
        For the given initial page we are going collect all the urls given in the result
        :param response:
        :return:
        '''
        logger.info("Requested: %s"%response.url)
        logger.info("Data looks like this\n%s"%response.body)

        #we are going to store these entries using an identificative url showing the time of the request
        now = datetime.datetime.now()
        urlToStore=response.url+"&"+datetime.datetime.strftime(now,"%Y%m%d%H%M")
        self.archiveItem(response,url=urlToStore)

        return




