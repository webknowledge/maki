

from mongoengine import Document,StringField,URLField,FileField,DateTimeField,connect
from django.conf import settings


res=connect(settings.ARCHIVE_DATABASE['archive'],
        host=settings.ARCHIVE_DATABASE['host'],
        port=settings.ARCHIVE_DATABASE['port']
        )


class ArchiveItem(Document):
    source = StringField(max_length=10,null=False)
    date = DateTimeField(null=False)
    url = StringField(null=False)
    contentType = StringField(null=False)
    file = FileField(null=False)
