
from django.core.management.base import BaseCommand,CommandError

from crawling.models import ArchiveItem
from mongoengine import DoesNotExist

import logging
# Get an instance of a logger
logger = logging.getLogger("MainLogger")

class Command(BaseCommand):
    help ='This command flushes the archival for a given datasource'
    
    def add_arguments(self,parser):
        parser.add_argument('source',type=str)
    
    def handle(self, *args, **options):
        self.launchParser(options['source'])
        
    def launchParser(self,source):
        answer = None
        while answer!="yes" and answer!="no":
            answer = raw_input("Are you really sure you want to reset the "+source+" archive?[yes|no]  ")
            answer=answer.strip()
        logger.info("Removing "+source+" archive")
        #Query pending items with source ted
        if answer=="no":
            logger.info("Reset cancelled")
            return
        try:
            pending = ArchiveItem.objects(source=source)
        except DoesNotExist:
            logger.error("No entries found for datasource "+source)
            return 
        counter = 0
        for p in pending:
            logger.info("Remove "+p.url)
            p.delete()
            counter+=1
        logger.info("We have removed "+str(counter)+" entries from the archive")
    