
from django.core.management.base import BaseCommand


from scrapy.settings import Settings

from twisted.internet import reactor
from django.conf import settings
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging

#this is used to import libraries in  python2.7 style
import pkgutil

import os

import logging



# Get an instance of a logger
logger = logging.getLogger("mainLogger")

#configure_logging({'LOG_LEVEL':'INFO'})
configure_logging({'LOG_LEVEL':'DEBUG'})


def startCrawlers(spider):
    os.environ['SCRAPY_SETTINGS_MODULE']=settings.SETTINGS_MODULE
    scrapySettings = Settings()
    settings_module_path=os.environ['SCRAPY_SETTINGS_MODULE']
    scrapySettings.setmodule(settings_module_path,priority='project')

    runner = CrawlerRunner()
    d=runner.crawl(spider)

    d.addBoth(lambda _:reactor.stop())
    reactor.run()


class Command(BaseCommand):
    
    help = 'Start the crawling process'
    
    def add_arguments(self,parser):
        parser.add_argument('source',type=str)
    
    def handle(self, *args, **options):
        self.launchCrawler(options['source'])
             
    def launchCrawler(self,source):


        #check if we have that crawler in the spiders section
        full_path="crawling.spiders.%s"%source
        loader = pkgutil.find_loader(full_path)
        found = loader is not None

        if found:
            aux=loader.load_module('Spider')
            startCrawlers(aux.Spider)
        else:
            logger.error("Requested crawler %s not found."%source)


